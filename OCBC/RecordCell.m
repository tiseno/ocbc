//
//  RecordCell.m
//  OCBC
//
//  Created by tiseno on 12/27/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "RecordCell.h"

@implementation RecordCell

@synthesize nolabel, datelabel, purposelabel, individuallabel, divisionlabel, ratinglabel, feedbacklabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *Nolabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.origin.y, 30, self.frame.size.height)];
        Nolabel.textAlignment = UITextAlignmentCenter;
        Nolabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Nolabel.backgroundColor = [UIColor clearColor];
        Nolabel.font = [UIFont systemFontOfSize:12];
        Nolabel.lineBreakMode = UILineBreakModeWordWrap;
        Nolabel.layer.borderWidth = 0.5;
        Nolabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Nolabel.numberOfLines = 1;
        self.nolabel = Nolabel;
        [Nolabel release];
        [self addSubview:nolabel];
        
        UILabel *Datelabel = [[UILabel alloc] initWithFrame:CGRectMake(self.nolabel.frame.origin.x + self.nolabel.frame.size.width, self.frame.origin.y, 80, self.frame.size.height)];
        Datelabel.textAlignment = UITextAlignmentCenter;
        Datelabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Datelabel.backgroundColor = [UIColor clearColor];
        Datelabel.font = [UIFont systemFontOfSize:12];
        Datelabel.lineBreakMode = UILineBreakModeWordWrap;
        Datelabel.layer.borderWidth = 0.5;
        Datelabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Datelabel.numberOfLines = 1;
        self.datelabel = Datelabel;
        [Datelabel release];
        [self addSubview:datelabel];
        
        UILabel *Purposelabel = [[UILabel alloc] initWithFrame:CGRectMake(self.datelabel.frame.origin.x + self.datelabel.frame.size.width, self.frame.origin.y, 80, self.frame.size.height)];
        Purposelabel.textAlignment = UITextAlignmentCenter;
        Purposelabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Purposelabel.backgroundColor = [UIColor clearColor];
        Purposelabel.font = [UIFont systemFontOfSize:12];
        Purposelabel.lineBreakMode = UILineBreakModeWordWrap;
        Purposelabel.layer.borderWidth = 0.5;
        Purposelabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Purposelabel.numberOfLines = 1;
        self.purposelabel = Purposelabel;
        [Purposelabel release];
        [self addSubview:purposelabel];
        
        UILabel *Individuallabel = [[UILabel alloc] initWithFrame:CGRectMake(self.purposelabel.frame.origin.x + self.purposelabel.frame.size.width, self.frame.origin.y, 80, self.frame.size.height)];
        Individuallabel.textAlignment = UITextAlignmentCenter;
        Individuallabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Individuallabel.backgroundColor = [UIColor clearColor];
        Individuallabel.font = [UIFont systemFontOfSize:12];
        Individuallabel.lineBreakMode = UILineBreakModeWordWrap;
        Individuallabel.layer.borderWidth = 0.5;
        Individuallabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Individuallabel.numberOfLines = 1;
        self.individuallabel = Individuallabel;
        [Individuallabel release];
        [self addSubview:individuallabel];
        
        UILabel *Divisionlabel = [[UILabel alloc] initWithFrame:CGRectMake(self.individuallabel.frame.origin.x + self.individuallabel.frame.size.width, self.frame.origin.y, 120, self.frame.size.height)];
        Divisionlabel.textAlignment = UITextAlignmentCenter;
        Divisionlabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Divisionlabel.backgroundColor = [UIColor clearColor];
        Divisionlabel.font = [UIFont systemFontOfSize:12];
        Divisionlabel.lineBreakMode = UILineBreakModeWordWrap;
        Divisionlabel.layer.borderWidth = 0.5;
        Divisionlabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Divisionlabel.numberOfLines = 0;
        self.divisionlabel = Divisionlabel;
        [Divisionlabel release];
        [self addSubview:divisionlabel];
        
        UILabel *Ratinglabel = [[UILabel alloc] initWithFrame:CGRectMake(self.divisionlabel.frame.origin.x + self.divisionlabel.frame.size.width, self.frame.origin.y, 120, self.frame.size.height)];
        Ratinglabel.textAlignment = UITextAlignmentCenter;
        Ratinglabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Ratinglabel.backgroundColor = [UIColor clearColor];
        Ratinglabel.font = [UIFont systemFontOfSize:12];
        Ratinglabel.lineBreakMode = UILineBreakModeWordWrap;
        Ratinglabel.layer.borderWidth = 0.5;
        Ratinglabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Ratinglabel.numberOfLines = 1;
        self.ratinglabel = Ratinglabel;
        [Ratinglabel release];
        [self addSubview:ratinglabel];
        
        UILabel *Feedbacklabel = [[UILabel alloc] initWithFrame:CGRectMake(self.ratinglabel.frame.origin.x + self.ratinglabel.frame.size.width, self.frame.origin.y, 200, self.frame.size.height)];
        Feedbacklabel.textAlignment = UITextAlignmentCenter;
        Feedbacklabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Feedbacklabel.backgroundColor = [UIColor clearColor];
        Feedbacklabel.font = [UIFont systemFontOfSize:12];
        Feedbacklabel.lineBreakMode = UILineBreakModeWordWrap;
        Feedbacklabel.layer.borderWidth = 0.5;
        Feedbacklabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
        Feedbacklabel.numberOfLines = 0;
        self.feedbacklabel = Feedbacklabel;
        [Feedbacklabel release];
        [self addSubview:feedbacklabel];
    }
    return self;
}

-(void)dealloc
{
    [nolabel release];
    [datelabel release];
    [purposelabel release];
    [individuallabel release];
    [divisionlabel release];
    [ratinglabel release];
    [feedbacklabel release];
    
    [super dealloc];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end

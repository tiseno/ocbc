//
//  MainScene.h
//  OCBC
//
//  Created by tiseno on 12/13/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LandscapeScene.h"
#import "PortraitScene.h"

@interface MainScene : UIViewController
{
    
}

@property (nonatomic, retain) PortraitScene *portrait;
@property (nonatomic, retain) LandscapeScene *landscape;

@end

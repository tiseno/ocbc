//
//  LandscapeScene.m
//  OCBC
//
//  Created by tiseno on 12/13/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#define AnimationDuration   0.3
#define KeyboardLandscape 352

#import "LandscapeScene.h"
#import "ViewController.h"

@implementation LandscapeScene

@synthesize employeepopview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    db = [[DatabaseAction alloc] init];
    
    appdelegate = [[UIApplication sharedApplication] delegate];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default_bg.png"]];
    
    logoimage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo.png"]];
    logoimage.frame = CGRectMake(0, 15, logoimage.image.size.width, logoimage.image.size.height);
    logoimage.userInteractionEnabled = YES;
    UISwipeGestureRecognizer *rightRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)] autorelease];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    [logoimage addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)] autorelease];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftRecognizer setNumberOfTouchesRequired:1];
    [logoimage addGestureRecognizer:leftRecognizer];
    [self.view addSubview:logoimage];
    [logoimage release];
    
    previousbutton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 245, 25, 81, 50)];
    [previousbutton setBackgroundImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [previousbutton addTarget:self action:@selector(previous:) forControlEvents:UIControlEventTouchUpInside];
    previousbutton.enabled = NO;
    [self.view addSubview:previousbutton];
    [previousbutton release];
    
    homebutton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 155, 22, 56, 56)];
    [homebutton setBackgroundImage:[UIImage imageNamed:@"btn_home.png"] forState:UIControlStateNormal];
    [homebutton addTarget:self action:@selector(home:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:homebutton];
    [homebutton release];
    
    nextbutton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 90, 25, 81, 50)];
    [nextbutton setBackgroundImage:[UIImage imageNamed:@"btn_next.png"] forState:UIControlStateNormal];
    [nextbutton addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
    nextbutton.enabled = NO;
    [self.view addSubview:nextbutton];
    [nextbutton release];
    
    questionview = [[UIView alloc]initWithFrame:CGRectMake(-self.view.frame.size.width, 260, self.view.frame.size.width, 60)];
    questionview.backgroundColor = [UIColor clearColor];
    questiontransform = questionview.transform;
    
    [self.view addSubview:questionview];
    [questionview release];
    
    answerview = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 350)];
    answerview.backgroundColor = [UIColor clearColor];
    answertransform = answerview.transform;
    
    [self.view addSubview:answerview];
    [answerview release];
    
    [self setemployeeepopview];
    
    [self prepareQA];
    
    [self performSelector:@selector(questionviewanimationin) withObject:nil afterDelay:0.5];
    [self performSelector:@selector(answerviewanimationin) withObject:nil afterDelay:0.5];
    
    if(appdelegate.numberofswipe == 3)
    {
        [self passwordview];
    }
    else
    {
        appdelegate.numberofswipe = 0;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
}

-(void)dealloc
{
    [employeepopview release];
    [_viewController release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)home:(id)sender
{
    for(int i = 0; i < appdelegate.answerselected.count; i++)
    {
        [appdelegate.answerselected replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:0]];
    }
    appdelegate.noofquestion = 1;
    self.viewController =[[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    appdelegate.window.rootViewController = self.viewController;
}

-(IBAction)previous:(id)sender
{
    self.employeepopview.hidden = YES;
    appdelegate.numberofswipe = 0;
    appdelegate.noofquestion--;
    
    if(appdelegate.noofquestion == 4)
    {
        if(commenttextview.isFirstResponder)
            [commenttextview resignFirstResponder];
    }
    
    [self prepareQA];
    [self questionviewanimationout];
    [self answerviewanimationout];
    [self performSelector:@selector(questionviewanimationin) withObject:nil afterDelay:0.2];
    [self performSelector:@selector(answerviewanimationin) withObject:nil afterDelay:0.2];
}

-(void)next
{
    self.employeepopview.hidden = YES;
    appdelegate.numberofswipe = 0;
    appdelegate.noofquestion++;
    [self prepareQA];
    [self questionviewanimationout];
    [self answerviewanimationout];
    [self performSelector:@selector(questionviewanimationin) withObject:nil afterDelay:0.2];
    [self performSelector:@selector(answerviewanimationin) withObject:nil afterDelay:0.2];
}

-(void)questionviewanimationin
{
    for(UIView *view in questionview.subviews)
        [view removeFromSuperview];
    
    if(appdelegate.noofquestion < 4)
    {
        UILabel *questionnolabel = [[UILabel alloc]initWithFrame:CGRectMake((questionview.frame.size.width - 300 - 600) / 2, 0, 300, questionview.frame.size.height)];
        questionnolabel.backgroundColor = [UIColor clearColor];
        questionnolabel.textAlignment = UITextAlignmentRight;
        questionnolabel.textColor = [UIColor colorWithRed:220.0/255.0 green:35.0/255.0 blue:26.0/255.0 alpha:1];
        questionnolabel.font = [UIFont boldSystemFontOfSize:24];
        questionnolabel.text = [NSString stringWithFormat:@"Question %d :", appdelegate.noofquestion];
        [questionview addSubview:questionnolabel];
        [questionnolabel release];
        
        UILabel *questiontextlabel = [[UILabel alloc]initWithFrame:CGRectMake((questionview.frame.size.width - 300 - 600) / 2 + 310, 0, 600, questionview.frame.size.height)];
        questiontextlabel.backgroundColor = [UIColor clearColor];
        questiontextlabel.textAlignment = UITextAlignmentLeft;
        questiontextlabel.textColor = [UIColor colorWithRed:90.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1];
        questiontextlabel.font = [UIFont boldSystemFontOfSize:24];
        questiontextlabel.text = [NSString stringWithFormat:@"%@", [appdelegate.questiontext objectAtIndex:appdelegate.noofquestion - 1]];
        [questionview addSubview:questiontextlabel];
        [questiontextlabel release];
    }
    else if(appdelegate.noofquestion == 4)
    {
        if([[appdelegate.answerselected objectAtIndex:2] integerValue] == 3)
        {
            //not meet
            UIImageView *whyimageview = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"why.png"]];
            whyimageview.frame = CGRectMake((questionview.frame.size.width - whyimageview.image.size.width) / 2, 0, whyimageview.image.size.width, whyimageview.image.size.height);
            [questionview addSubview:whyimageview];
            [whyimageview release];
        }
        else
        {
            //meet
            if(isthankyou)
            {
                UIImageView *whyimageview = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"thank_you.png"]];
                whyimageview.frame = CGRectMake((questionview.frame.size.width - whyimageview.image.size.width) / 2, 0, whyimageview.image.size.width, whyimageview.image.size.height);
                [questionview addSubview:whyimageview];
                [whyimageview release];
            }
        }
    }
    
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:AnimationDuration];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(questionview.frame.size.width, 0);
        questionview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             questiontransform = questionview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)questionviewanimationout
{
    questionview.transform = questiontransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:AnimationDuration];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(-questionview.frame.size.width, 0);
        questionview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             questiontransform = questionview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)answerviewanimationin
{
    for(UIView *view in answerview.subviews)
        [view removeFromSuperview];
    
    if(appdelegate.noofquestion < 4)
    {
        for(int i = 1; i <= appdelegate.noofanswer; i++)
        {
            UIView *answerbuttonview = [[UIView alloc]init];
            
            UIImageView *selectedindigatorimage = [[UIImageView alloc]init];
            
            UIButton *answerbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 147, 148)];
            [answerbutton setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"btn_q%da%d", appdelegate.noofquestion, i]] forState:UIControlStateNormal];
            [answerbutton addTarget:self action:@selector(answer:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *answertext = [[UILabel alloc]initWithFrame:CGRectMake(0, answerbutton.frame.size.height, answerbutton.frame.size.width, 20)];
            answertext.backgroundColor = [UIColor clearColor];
            answertext.textAlignment = UITextAlignmentCenter;
            answertext.textColor = [UIColor colorWithRed:223.0/255.0 green:63.0/255.0 blue:63.0/255.0 alpha:1];
            answertext.font = [UIFont systemFontOfSize:13];
            
            if(appdelegate.noofquestion == 1)
            {
                answertext.text = [NSString stringWithFormat:@"%@", [appdelegate.answertext objectAtIndex: -1 + i]];
                answerbutton.tag = i + 100;
                selectedindigatorimage.tag = i + 150;
            }
            else if(appdelegate.noofquestion == 2)
            {
                answertext.text = [NSString stringWithFormat:@"%@", [appdelegate.answertext objectAtIndex: 3 + i]];
                answerbutton.tag = i + 200;
                selectedindigatorimage.tag = i + 250;
            }
            else if(appdelegate.noofquestion == 3)
            {
                answertext.text = [NSString stringWithFormat:@"%@", [appdelegate.answertext objectAtIndex: 5 + i]];
                answerbutton.tag = i + 300;
                selectedindigatorimage.tag = i + 350;
            }
            
            float gapX = (answerview.frame.size.width - (appdelegate.noofanswer * answerbutton.frame.size.width)) / (appdelegate.noofanswer + 1);
            
            answerbuttonview.frame = CGRectMake(gapX + ((answerbutton.frame.size.width + gapX) * (i - 1)), (answerview.frame.size.height - answerbutton.frame.size.height - answertext.frame.size.height) / 2, answerbutton.frame.size.width, answerbutton.frame.size.height + answertext.frame.size.height);
            answerbuttonview.backgroundColor = [UIColor clearColor];
            
            selectedindigatorimage.image = [UIImage imageNamed:@"icon_selected.png"];
            selectedindigatorimage.frame = CGRectMake(answerbutton.frame.size.width - selectedindigatorimage.image.size.width, answerbutton.frame.origin.y, selectedindigatorimage.image.size.width, selectedindigatorimage.image.size.height);
            selectedindigatorimage.hidden = YES;
            
            [answerbuttonview addSubview:answerbutton];
            [answerbutton release];
            
            [answerbuttonview addSubview:answertext];
            [answertext release];
            
            [answerbuttonview addSubview:selectedindigatorimage];
            [selectedindigatorimage release];
            
            [answerview addSubview:answerbuttonview];
            [answerbuttonview release];
        }
    }
    else if(appdelegate.noofquestion == 4)
    {
        if([[appdelegate.answerselected objectAtIndex:2] integerValue] == 3 || isthankyou)
        {
            //not meet
            commenttextview = [[UITextView alloc]initWithFrame:CGRectMake(answerview.frame.size.width / 8, 20, answerview.frame.size.width * 3 / 4, answerview.frame.size.height / 4)];
            commenttextview.delegate = self;
            commenttextview.font = [UIFont systemFontOfSize:15];
            commenttextview.layer.borderWidth = 1.5;
            commenttextview.layer.cornerRadius = 5;
            commenttextview.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5] CGColor];
            
            [answerview addSubview:commenttextview];
            [commenttextview release];
            
            UIButton *cancelbutton = [[UIButton alloc]initWithFrame:CGRectMake((answerview.frame.size.width - 139 * 2 - 15) / 2, answerview.frame.size.height * 2 / 5 - 5, 139, 44)];
            [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
            [cancelbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelbutton addTarget:self action:@selector(cancelwhy:) forControlEvents:UIControlEventTouchUpInside];
            cancelbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_grey.png"]];
            
            [answerview addSubview:cancelbutton];
            [cancelbutton release];
            
            UIButton *submitbutton = [[UIButton alloc]initWithFrame:CGRectMake((answerview.frame.size.width - 139 * 2 - 15) / 2 + 154, answerview.frame.size.height * 2 / 5 - 5, 139, 44)];
            [submitbutton setTitle:@"Submit" forState:UIControlStateNormal];
            [submitbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [submitbutton addTarget:self action:@selector(submitwhy:) forControlEvents:UIControlEventTouchUpInside];
            submitbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_red.png"]];
            
            [answerview addSubview:submitbutton];
            [submitbutton release];
        }
        else
        {
            UIButton *commentbutton = [[UIButton alloc]initWithFrame:CGRectMake((answerview.frame.size.width - 139 * 2 - 15) / 2, (answerview.frame.size.height - 44) / 2, 139, 44)];
            [commentbutton setTitle:@"Comment" forState:UIControlStateNormal];
            [commentbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [commentbutton addTarget:self action:@selector(commentthankyou:) forControlEvents:UIControlEventTouchUpInside];
            commentbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_grey.png"]];
            
            [answerview addSubview:commentbutton];
            [commentbutton release];
            
            UIButton *endbutton = [[UIButton alloc]initWithFrame:CGRectMake((answerview.frame.size.width - 139 * 2 - 15) / 2 + 154, (answerview.frame.size.height - 44) / 2, 139, 44)];
            [endbutton setTitle:@"End" forState:UIControlStateNormal];
            [endbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [endbutton addTarget:self action:@selector(end:) forControlEvents:UIControlEventTouchUpInside];
            endbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_red.png"]];
            
            [answerview addSubview:endbutton];
            [endbutton release];
        }
    }
    else
    {
        UIButton *endbutton = [[UIButton alloc]initWithFrame:CGRectMake((answerview.frame.size.width - 139) / 2, (answerview.frame.size.height - 44) / 2, 139, 44)];
        [endbutton setTitle:@"End" forState:UIControlStateNormal];
        [endbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [endbutton addTarget:self action:@selector(end:) forControlEvents:UIControlEventTouchUpInside];
        endbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_red.png"]];
        
        [answerview addSubview:endbutton];
        [endbutton release];
    }
    
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:AnimationDuration];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -answerview.frame.size.height);
        answerview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             answertransform = answerview.transform;
                             [self indicatorlocation];
                         }
                     }];
    [UIView commitAnimations];
}

-(void)answerviewanimationout
{
    answerview.transform = answertransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:AnimationDuration];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(0, answerview.frame.size.height);
        answerview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             answertransform = answerview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)prepareQA
{
    if(appdelegate.noofquestion < 4)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default_bg.png"]];
        currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
        previousbutton.hidden = NO;
        nextbutton.hidden = NO;
        
        if(appdelegate.noofquestion == 1)
        {
            appdelegate.noofanswer = 4;
            previousbutton.enabled = NO;
            if(currentselection > 0)
                nextbutton.enabled = YES;
            else
                nextbutton.enabled = NO;
        }
        else if(appdelegate.noofquestion == 2)
        {
            appdelegate.noofanswer = 2;
            previousbutton.enabled = YES;
            if(currentselection > 0)
                nextbutton.enabled = YES;
            else
                nextbutton.enabled = NO;
        }
        else if(appdelegate.noofquestion == 3)
        {
            appdelegate.noofanswer = 3;
            previousbutton.enabled = YES;
            if(currentselection > 0)
                nextbutton.enabled = YES;
            else
                nextbutton.enabled = NO;
        }
    }
    else
    {
        if(appdelegate.noofquestion == 4)
        {
            if([[appdelegate.answerselected objectAtIndex:2] integerValue] == 3)
            {
                self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default_bg.png"]];
            }
            else
            {
                if(isthankyou)
                {
                    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default_bg.png"]];
                }
                else
                {
                    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"thank_you_page.png"]];
                }
            }
            previousbutton.enabled = YES;
            nextbutton.enabled = NO;
        }
        
        if(appdelegate.noofquestion == 5)
        {
            if([[appdelegate.answerselected objectAtIndex:2] integerValue] == 3)
            {
                self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"thank_you_page2.png"]];
            }
            else
            {
                self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"thank_you_page.png"]];
            }
            previousbutton.hidden = YES;
            nextbutton.hidden = YES;
        }
    }
}

-(IBAction)answer:(id)sender
{
    if(appdelegate.noofquestion == 1)
    {
        [appdelegate.answerselected replaceObjectAtIndex:appdelegate.noofquestion - 1 withObject:[NSNumber numberWithInt:[sender tag] - 100]];
        currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
        for(int i = 1; i <= 4; i++)
        {
            UIImageView *imageview = (UIImageView*)[answerview viewWithTag:i + 150];
            if(i != currentselection)
            {
                imageview.hidden = YES;
            }
            else
            {
                imageview.hidden = NO;
            }
        }
        nextbutton.enabled = YES;
        [self next];
    }
    else if(appdelegate.noofquestion == 2)
    {
        if([sender tag] == 201)
        {
            self.employeepopview.hidden = NO;
        }
        else
        {
            [appdelegate.answerselected replaceObjectAtIndex:appdelegate.noofquestion - 1 withObject:[NSNumber numberWithInt:[sender tag] - 200]];
            currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
            for(int i = 1; i <= 2; i++)
            {
                UIImageView *imageview = (UIImageView*)[answerview viewWithTag:i + 250];
                
                if(i != currentselection)
                {
                    imageview.hidden = YES;
                }
                else
                {
                    imageview.hidden = NO;
                }
            }
            
            appdelegate.selection = -1;
            self.employeepopview.hidden = YES;
            nextbutton.enabled = YES;
            [divisiontableview reloadData];
            donebutton.enabled = NO;
            [self next];
        }
    }
    else if(appdelegate.noofquestion == 3)
    {
        [appdelegate.answerselected replaceObjectAtIndex:appdelegate.noofquestion - 1 withObject:[NSNumber numberWithInt:[sender tag] - 300]];
        currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
        for(int i = 1; i <= 3; i++)
        {
            UIImageView *imageview = (UIImageView*)[answerview viewWithTag:i + 350];
            if(i != currentselection)
            {
                imageview.hidden = YES;
            }
            else
            {
                imageview.hidden = NO;
            }
        }
        
        nextbutton.enabled = YES;
        [self next];
    }
}

-(void)indicatorlocation
{
    if(appdelegate.noofquestion == 1)
    {
        currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
        if(currentselection > 0)
        {
            for(int i = 1; i <= 4; i++)
            {
                UIImageView *imageview = (UIImageView*)[answerview viewWithTag:i + 150];
                if(i != currentselection)
                {
                    imageview.hidden = YES;
                }
                else
                {
                    imageview.hidden = NO;
                }
            }
        }
    }
    else if(appdelegate.noofquestion == 2)
    {
        currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
        if(currentselection > 0)
        {
            for(int i = 1; i <= 2; i++)
            {
                UIImageView *imageview = (UIImageView*)[answerview viewWithTag:i + 250];
                if(i != currentselection)
                {
                    imageview.hidden = YES;
                }
                else
                {
                    imageview.hidden = NO;
                }
            }
        }
    }
    else if(appdelegate.noofquestion == 3)
    {
        currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
        if(currentselection > 0)
        {
            for(int i = 1; i <= 3; i++)
            {
                UIImageView *imageview = (UIImageView*)[answerview viewWithTag:i + 350];
                if(i != currentselection)
                {
                    imageview.hidden = YES;
                }
                else
                {
                    imageview.hidden = NO;
                }
            }
        }
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    answerview.frame = CGRectMake(0, answerview.frame.size.height - KeyboardLandscape * 2 / 5, answerview.frame.size.width, answerview.frame.size.height);
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    answerview.frame = CGRectMake(0, self.view.frame.size.height - answerview.frame.size.height, self.view.frame.size.width, answerview.frame.size.height);
    appdelegate.comment = commenttextview.text;
}

-(IBAction)submitwhy:(id)sender
{
    NSString *check = [commenttextview.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![check isEqualToString:@""])
    {
        [commenttextview resignFirstResponder];
        answerview.frame = CGRectMake(0, self.view.frame.size.height - answerview.frame.size.height, self.view.frame.size.width, answerview.frame.size.height);
        [self next];
    }
    else
    {
        [self.view makeToast:@"Please give us your feedback!" duration:(0.3) position:@"center"];
    }
}

-(IBAction)cancelwhy:(id)sender
{
    if(isthankyou)
    {
        isthankyou = NO;
    }
    else
    {
        appdelegate.noofquestion--;
    }
    [commenttextview resignFirstResponder];
    answerview.frame = CGRectMake(0, self.view.frame.size.height - answerview.frame.size.height, self.view.frame.size.width, answerview.frame.size.height);
    [self prepareQA];
    [self questionviewanimationout];
    [self answerviewanimationout];
    [self performSelector:@selector(questionviewanimationin) withObject:nil afterDelay:0.2];
    [self performSelector:@selector(answerviewanimationin) withObject:nil afterDelay:0.2];
}

-(IBAction)commentthankyou:(id)sender
{
    isthankyou = YES;
    [self prepareQA];
    [self questionviewanimationout];
    [self answerviewanimationout];
    [self performSelector:@selector(questionviewanimationin) withObject:nil afterDelay:0.2];
    [self performSelector:@selector(answerviewanimationin) withObject:nil afterDelay:0.2];
}


-(IBAction)end:(id)sender
{
    survey = [[Survey alloc]init];
    
    survey.createddate = [self getCurrentTime];
    
    survey.question1 = [appdelegate.answertext objectAtIndex:[[appdelegate.answerselected objectAtIndex:0] integerValue]-1];
    
    if([[appdelegate.answerselected objectAtIndex:1] integerValue] == 1)
    {
        survey.division = [NSString stringWithFormat:@"%@", [appdelegate.divisiontext objectAtIndex:appdelegate.selection]];
    }
    else
    {
        survey.division = @"N/A";
    }
    
    survey.question2 = [appdelegate.answertext objectAtIndex:4 + [[appdelegate.answerselected objectAtIndex:1] integerValue]-1];
    
    survey.question3 = [appdelegate.answertext objectAtIndex:6 + [[appdelegate.answerselected objectAtIndex:2] integerValue]-1];
    
    NSString *check = [appdelegate.comment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![check isEqualToString:@""] && appdelegate.comment != nil)
    {
        survey.comment = appdelegate.comment;
    }
    else
    {
        survey.comment = @"N/A";
    }
    
    [db insertSurvey:survey];
    [survey release];
    
    for(int i = 0; i < appdelegate.answerselected.count; i++)
    {
        [appdelegate.answerselected replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:0]];
    }
    appdelegate.noofquestion = 1;
    self.viewController =[[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    appdelegate.window.rootViewController = self.viewController;
}

-(NSString*)getCurrentTime
{
    NSFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *currentDate = [NSDate date];
    
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    NSString *DateinString = [formatter stringFromDate:currentDate];
    
    [formatter release];
    
    return DateinString;
}

-(void)setemployeeepopview
{
    self.employeepopview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"table_division.png"]];
    self.employeepopview.frame = CGRectMake(95, 150, self.employeepopview.frame.size.width, self.employeepopview.frame.size.height);
    self.employeepopview.hidden = YES;
    
    UIButton *cancelbutton = [[UIButton alloc]initWithFrame:CGRectMake(65, 55, 65, 34)];
    [cancelbutton addTarget:self action:@selector(popbuttoncancel:) forControlEvents:UIControlEventTouchUpInside];
    cancelbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_cancel.png"]];
    
    donebutton = [[UIButton alloc]initWithFrame:CGRectMake(self.employeepopview.frame.size.width - 130, 55, 65, 34)];
    [donebutton addTarget:self action:@selector(popbuttondone:) forControlEvents:UIControlEventTouchUpInside];
    donebutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_done.png"]];
    donebutton.enabled = NO;
    
    divisiontableview = [[UITableView alloc]initWithFrame:CGRectMake(65, 96, 320, 220)];
    divisiontableview.backgroundColor = [UIColor clearColor];
    divisiontableview.dataSource = self;
    divisiontableview.delegate = self;
    
    [self.employeepopview addSubview:cancelbutton];
    [cancelbutton release];
    
    [self.employeepopview addSubview:donebutton];
    [donebutton release];
    
    [self.employeepopview addSubview:divisiontableview];
    [divisiontableview release];
    
    [self.view addSubview:self.employeepopview];
}

-(IBAction)popbuttondone:(id)sender
{
    self.employeepopview.hidden = YES;
    
    [appdelegate.answerselected replaceObjectAtIndex:appdelegate.noofquestion - 1 withObject:[NSNumber numberWithInt:1]];
    currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
        
    nextbutton.enabled = YES;
    UIImageView *imageview = (UIImageView*)[answerview viewWithTag:251];
    imageview.hidden = NO;
        
    UIImageView *imageview2 = (UIImageView*)[answerview viewWithTag:252];
    imageview2.hidden = YES;
    
    [self next];
}

-(IBAction)popbuttoncancel:(id)sender
{
    self.employeepopview.hidden = YES;
    currentselection = [[appdelegate.answerselected objectAtIndex:appdelegate.noofquestion - 1] integerValue];
    if(currentselection == 0)
        nextbutton.enabled = NO;
    else
        nextbutton.enabled = YES;
    
    UIImageView *imageview = (UIImageView*)[answerview viewWithTag:251];
    imageview.hidden = YES;
    
    appdelegate.selection = -1;
    [divisiontableview reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == recordtableview)
    {
        surveyarray = [db retrieveSurvey];
        return surveyarray.count;
    }
    else
        return appdelegate.divisiontext.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == recordtableview)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        recordcell = (RecordCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(recordcell == nil){
            recordcell = [[[RecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        recordcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        surveyarray = [db retrieveSurvey];
        
        survey = [surveyarray objectAtIndex:indexPath.row];
        recordcell.nolabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
        recordcell.datelabel.text = survey.createddate;
        recordcell.purposelabel.text = survey.question1;
        recordcell.individuallabel.text = survey.question2;
        recordcell.divisionlabel.text = survey.division;
        recordcell.ratinglabel.text = survey.question3;
        recordcell.feedbacklabel.text = survey.comment;
        
        CGSize maxsize = CGSizeMake(120, 9999);
        UIFont *thefont = [UIFont systemFontOfSize:12];
        CGSize textsize = [survey.division sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        recordcell.divisionlabel.frame = CGRectMake(recordcell.divisionlabel.frame.origin.x, recordcell.divisionlabel.frame.origin.y, textsize.width, textsize.height);
        
        CGSize size = CGSizeMake(200, 9999);
        CGSize sizetext = [survey.comment sizeWithFont:thefont constrainedToSize:size lineBreakMode:UILineBreakModeWordWrap];
        
        recordcell.feedbacklabel.frame = CGRectMake(recordcell.feedbacklabel.frame.origin.x, recordcell.feedbacklabel.frame.origin.y, sizetext.width, sizetext.height);
        
        float height;
        
        if(textsize.height > sizetext.height)
        {
            height = textsize.height + 5;
        }
        else
        {
            height = sizetext.height + 5;
        }
        
        recordcell.frame = CGRectMake(0, 0, 710, height);
        
        recordcell.nolabel.frame = CGRectMake(recordcell.nolabel.frame.origin.x, recordcell.nolabel.frame.origin.y, recordcell.nolabel.frame.size.width, height);
        
        recordcell.datelabel.frame = CGRectMake(recordcell.datelabel.frame.origin.x, recordcell.datelabel.frame.origin.y, recordcell.datelabel.frame.size.width, height);
        
        recordcell.purposelabel.frame = CGRectMake(recordcell.purposelabel.frame.origin.x, recordcell.purposelabel.frame.origin.y, recordcell.purposelabel.frame.size.width, height);
        
        recordcell.individuallabel.frame = CGRectMake(recordcell.individuallabel.frame.origin.x, recordcell.individuallabel.frame.origin.y, recordcell.individuallabel.frame.size.width, height);
        
        recordcell.divisionlabel.frame = CGRectMake(recordcell.divisionlabel.frame.origin.x, recordcell.divisionlabel.frame.origin.y, 120, height);
        
        recordcell.ratinglabel.frame = CGRectMake(recordcell.ratinglabel.frame.origin.x, recordcell.ratinglabel.frame.origin.y, recordcell.ratinglabel.frame.size.width, height);
        
        recordcell.feedbacklabel.frame = CGRectMake(recordcell.feedbacklabel.frame.origin.x, recordcell.feedbacklabel.frame.origin.y, 200, height);
        return recordcell;
    }
    else
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        divisioncell = (DivisionCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(divisioncell == nil){
            divisioncell = [[[DivisionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        divisiontableview.layer.borderWidth = 0.5;
        divisiontableview.layer.cornerRadius = 3;
        divisiontableview.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.7].CGColor;
        
        divisioncell.frame = CGRectMake(0, 0, divisiontableview.frame.size.width, 37);
        
        divisioncell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        if(appdelegate.selection == indexPath.row)
        {
            divisioncell.tickimageview.hidden = NO;
        }
        else
        {
            divisioncell.tickimageview.hidden = YES;
        }
        
        divisioncell.titlelabel.text = [appdelegate.divisiontext objectAtIndex:indexPath.row];
        
        return divisioncell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    divisioncell = (DivisionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    appdelegate.selection = indexPath.row;
    isdraw = NO;
    [divisiontableview reloadData];
    donebutton.enabled = YES;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    if(tableView == recordtableview)
    {
        recordcell = (RecordCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return recordcell.frame.size.height;
    }
    else
    {
        divisioncell = (DivisionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return divisioncell.frame.size.height;
    }
}

-(UIView*)tableheader
{
    UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, recordtableview.frame.size.width, 18)];
    
    UILabel *Nolabel = [[UILabel alloc] initWithFrame:CGRectMake(headerview.frame.origin.x, headerview.frame.origin.y, 30, headerview.frame.size.height)];
    Nolabel.text = @"No";
    Nolabel.textAlignment = UITextAlignmentCenter;
    Nolabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Nolabel.backgroundColor = [UIColor clearColor];
    Nolabel.font = [UIFont boldSystemFontOfSize:12];
    Nolabel.lineBreakMode = UILineBreakModeWordWrap;
    Nolabel.layer.borderWidth = 0.5;
    Nolabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Nolabel.numberOfLines = 1;
    
    UILabel *Datelabel = [[UILabel alloc] initWithFrame:CGRectMake(Nolabel.frame.origin.x + Nolabel.frame.size.width, headerview.frame.origin.y, 80, headerview.frame.size.height)];
    Datelabel.text = @"Date";
    Datelabel.textAlignment = UITextAlignmentCenter;
    Datelabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Datelabel.backgroundColor = [UIColor clearColor];
    Datelabel.font = [UIFont boldSystemFontOfSize:12];
    Datelabel.lineBreakMode = UILineBreakModeWordWrap;
    Datelabel.layer.borderWidth = 0.5;
    Datelabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Datelabel.numberOfLines = 1;
    
    UILabel *Purposelabel = [[UILabel alloc] initWithFrame:CGRectMake(Datelabel.frame.origin.x + Datelabel.frame.size.width, headerview.frame.origin.y, 80, headerview.frame.size.height)];
    Purposelabel.text = @"Purpose";
    Purposelabel.textAlignment = UITextAlignmentCenter;
    Purposelabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Purposelabel.backgroundColor = [UIColor clearColor];
    Purposelabel.font = [UIFont boldSystemFontOfSize:12];
    Purposelabel.lineBreakMode = UILineBreakModeWordWrap;
    Purposelabel.layer.borderWidth = 0.5;
    Purposelabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Purposelabel.numberOfLines = 1;
    
    UILabel *Individuallabel = [[UILabel alloc] initWithFrame:CGRectMake(Purposelabel.frame.origin.x + Purposelabel.frame.size.width, headerview.frame.origin.y, 80, headerview.frame.size.height)];
    Individuallabel.text = @"Individual";
    Individuallabel.textAlignment = UITextAlignmentCenter;
    Individuallabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Individuallabel.backgroundColor = [UIColor clearColor];
    Individuallabel.font = [UIFont boldSystemFontOfSize:12];
    Individuallabel.lineBreakMode = UILineBreakModeWordWrap;
    Individuallabel.layer.borderWidth = 0.5;
    Individuallabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Individuallabel.numberOfLines = 1;
    
    UILabel *Divisionlabel = [[UILabel alloc] initWithFrame:CGRectMake(Individuallabel.frame.origin.x + Individuallabel.frame.size.width, headerview.frame.origin.y, 120, headerview.frame.size.height)];
    Divisionlabel.text = @"Division";
    Divisionlabel.textAlignment = UITextAlignmentCenter;
    Divisionlabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Divisionlabel.backgroundColor = [UIColor clearColor];
    Divisionlabel.font = [UIFont boldSystemFontOfSize:12];
    Divisionlabel.lineBreakMode = UILineBreakModeWordWrap;
    Divisionlabel.layer.borderWidth = 0.5;
    Divisionlabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Divisionlabel.numberOfLines = 1;
    
    UILabel *Ratinglabel = [[UILabel alloc] initWithFrame:CGRectMake(Divisionlabel.frame.origin.x + Divisionlabel.frame.size.width, headerview.frame.origin.y, 120, headerview.frame.size.height)];
    Ratinglabel.text = @"Rating";
    Ratinglabel.textAlignment = UITextAlignmentCenter;
    Ratinglabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Ratinglabel.backgroundColor = [UIColor clearColor];
    Ratinglabel.font = [UIFont boldSystemFontOfSize:12];
    Ratinglabel.lineBreakMode = UILineBreakModeWordWrap;
    Ratinglabel.layer.borderWidth = 0.5;
    Ratinglabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Ratinglabel.numberOfLines = 1;
    
    UILabel *Feedbacklabel = [[UILabel alloc] initWithFrame:CGRectMake(Ratinglabel.frame.origin.x + Ratinglabel.frame.size.width, headerview.frame.origin.y, 200, headerview.frame.size.height)];
    Feedbacklabel.text = @"Feedback";
    Feedbacklabel.textAlignment = UITextAlignmentCenter;
    Feedbacklabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    Feedbacklabel.backgroundColor = [UIColor clearColor];
    Feedbacklabel.font = [UIFont boldSystemFontOfSize:12];
    Feedbacklabel.lineBreakMode = UILineBreakModeWordWrap;
    Feedbacklabel.layer.borderWidth = 0.5;
    Feedbacklabel.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5].CGColor;
    Feedbacklabel.numberOfLines = 0;
    
    [headerview addSubview:Nolabel];
    [Nolabel release];
    
    [headerview addSubview:Datelabel];
    [Datelabel release];
    
    [headerview addSubview:Purposelabel];
    [Purposelabel release];
    
    [headerview addSubview:Individuallabel];
    [Individuallabel release];
    
    [headerview addSubview:Divisionlabel];
    [Divisionlabel release];
    
    [headerview addSubview:Ratinglabel];
    [Ratinglabel release];
    
    [headerview addSubview:Feedbacklabel];
    [Feedbacklabel release];
    
    return [headerview autorelease];
}

- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    appdelegate.numberofswipe++;
    if(appdelegate.numberofswipe == 3)
    {
        [self passwordview];
    }
}

- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    appdelegate.numberofswipe++;
    if(appdelegate.numberofswipe == 3)
    {
        [self passwordview];
    }
}

-(void) singletapcaptured:(UIGestureRecognizer *)gesture
{
    CGPoint coords = [gesture locationInView:secretbgview];
    
    if (!CGRectContainsPoint(recordtableview.frame, coords))
    {
        [secretbgview removeGestureRecognizer:gesture];
        [recordtableview removeFromSuperview];
    }
}

-(void)secretview
{
    UIView *adminboxview = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width - 413) / 2 , (self.view.frame.size.height - 228) / 2, 413, 228)];
    adminboxview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"admin_ctrl_box.png"]];
    
    UILabel *adminlabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 70, adminboxview.frame.size.width - 60, 60)];
    adminlabel.backgroundColor = [UIColor clearColor];
    adminlabel.textAlignment = UITextAlignmentCenter;
    adminlabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    adminlabel.font = [UIFont boldSystemFontOfSize:14];
    adminlabel.numberOfLines = 0;
    surveyarray = [db retrieveSurvey];
    adminlabel.text = [NSString stringWithFormat:@"You have %d records.", surveyarray.count];
    [adminboxview addSubview:adminlabel];
    [adminlabel release];
    
    UIButton *cancelbutton = [[UIButton alloc]initWithFrame:CGRectMake(adminboxview.frame.size.width - 75, 10, 64, 35)];
    [cancelbutton addTarget:self action:@selector(admincancelbutton:) forControlEvents:UIControlEventTouchUpInside];
    cancelbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_admincancel.png"]];
    
    UIButton *viewbutton = [[UIButton alloc]initWithFrame:CGRectMake((adminboxview.frame.size.width - 290) / 2, 150, 67, 44)];
    [viewbutton addTarget:self action:@selector(viewbutton:) forControlEvents:UIControlEventTouchUpInside];
    viewbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_view.png"]];
    
    UIButton *sendbutton = [[UIButton alloc]initWithFrame:CGRectMake(viewbutton.frame.origin.x + viewbutton.frame.size.width + 10, 150, 67, 44)];
    [sendbutton addTarget:self action:@selector(adminsendbutton:) forControlEvents:UIControlEventTouchUpInside];
    sendbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_adminsend.png"]];
    
    UIButton *senddeletebutton = [[UIButton alloc]initWithFrame:CGRectMake(sendbutton.frame.origin.x + sendbutton.frame.size.width + 10, 150, 136, 43)];
    [senddeletebutton addTarget:self action:@selector(adminsenddeletebutton:) forControlEvents:UIControlEventTouchUpInside];
    senddeletebutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_adminsend_delete.png"]];
    
    if(surveyarray.count < 1)
    {
        sendbutton.enabled = NO;
        senddeletebutton.enabled = NO;
        viewbutton.enabled = NO;
    }
    else
    {
        sendbutton.enabled = YES;
        senddeletebutton.enabled = YES;
        viewbutton.enabled = YES;
    }
    
    [adminboxview addSubview:viewbutton];
    [viewbutton release];
    
    [adminboxview addSubview:cancelbutton];
    [cancelbutton release];
    
    [adminboxview addSubview:sendbutton];
    [sendbutton release];
    
    [adminboxview addSubview:senddeletebutton];
    [senddeletebutton release];
    
    [secretbgview addSubview:adminboxview];
    [adminboxview release];
}

-(void)passwordview
{
    secretbgview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    secretbgview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    passwordview = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width - 413) / 2 , (self.view.frame.size.height - 228) / 2 - 60, 413, 228)];
    passwordview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"admin_ctrl_box.png"]];
    
    passwordtextview = [[UITextField alloc] initWithFrame:CGRectMake((passwordview.frame.size.width - 260) / 2, (passwordview.frame.size.height - 25) / 2, 260.0, 25.0)];
    passwordtextview.secureTextEntry = YES;
    passwordtextview.placeholder=@"Enter Password";
    [passwordtextview becomeFirstResponder];
    [passwordtextview setBackgroundColor:[UIColor whiteColor]];
    passwordtextview.textAlignment=UITextAlignmentLeft;
    passwordtextview.layer.cornerRadius=5.0;
    passwordtextview.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5] CGColor];
    [passwordview addSubview:passwordtextview];
    [commenttextview release];
    
    UIButton *cancelbutton = [[UIButton alloc]initWithFrame:CGRectMake(passwordview.frame.size.width - 75, 10, 64, 35)];
    [cancelbutton addTarget:self action:@selector(admincancelbutton:) forControlEvents:UIControlEventTouchUpInside];
    cancelbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_admincancel.png"]];
    
    
    UIButton *okbutton = [[UIButton alloc]initWithFrame:CGRectMake((passwordview.frame.size.width - 139) / 2, 150, 139, 44)];
    [okbutton addTarget:self action:@selector(okbutton:) forControlEvents:UIControlEventTouchUpInside];
    [okbutton setTitle:@"OK" forState:UIControlStateNormal];
    okbutton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"btn_red.png"]];
    
    [passwordview addSubview:cancelbutton];
    [cancelbutton release];
    
    [passwordview addSubview:okbutton];
    [okbutton release];
    
    [secretbgview addSubview:passwordview];
    [passwordview release];
    
    [self.view addSubview:secretbgview];
    [secretbgview release];
}

-(IBAction)viewbutton:(id)sender
{
    recordtableview = [[UITableView alloc]initWithFrame:CGRectMake((secretbgview.frame.size.width - 710) / 2, (secretbgview.frame.size.height - 710) / 2, 710, 710)];
    recordtableview.backgroundColor = [UIColor clearColor];
    recordtableview.dataSource = self;
    recordtableview.delegate = self;
    recordtableview.backgroundColor = [UIColor whiteColor];
    recordtableview.tableFooterView = [[[UIView alloc]init] autorelease];
    recordtableview.tableHeaderView = [self tableheader];
    
    [secretbgview addSubview:recordtableview];
    [recordtableview release];
    
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singletapcaptured:)];
    [secretbgview addGestureRecognizer:singletap];
    [secretbgview setUserInteractionEnabled:YES];
    [singletap release];
}

-(IBAction)adminsendbutton:(id)sender
{
    [self generateCSV];
    isdelete = NO;
}

-(IBAction)adminsenddeletebutton:(id)sender
{
    [self warningmessage];
}

-(IBAction)admincancelbutton:(id)sender
{
    if(isdelete)
    {
        [db deleteSurvey];
        isdelete = NO;
    }
    
    appdelegate.numberofswipe = 0;
    [secretbgview removeFromSuperview];
}

-(IBAction)okbutton:(id)sender
{
    if([passwordtextview.text isEqualToString:@"hrcx"])
    {
        [passwordtextview resignFirstResponder];
        [passwordview removeFromSuperview];
        [self secretview];
    }
    else
        [self.view makeToast:@"Incorrect password!" duration:(0.3) position:@"center"];
}

-(void)warningmessage
{
    alert = [[UIAlertView alloc] initWithTitle:@"Are you sure want to send and delete records?" message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"admin_ctrl_box.png"]];
    
    backgroundImageView.frame = CGRectMake(-(413-282) / 2, -(218-130) / 2, 413, 228);
    [alert addSubview:backgroundImageView];
    
    [alert sendSubviewToBack:backgroundImageView];
    [alert show];
    [alert release];
    [backgroundImageView release];
    
    UILabel *theTitle = [alert valueForKey:@"_titleLabel"];
    [theTitle setShadowOffset:CGSizeMake(0, 0)];
    [theTitle setTextColor:[UIColor colorWithRed:90.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0]];
    
    UIButton *theButton = [alert valueForKey:@"_buttons"];
    [[theButton objectAtIndex:0] setTitleColor:[UIColor colorWithRed:90.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [[theButton objectAtIndex:1] setTitleColor:[UIColor colorWithRed:90.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0] forState:UIControlStateNormal];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }
    else
    {
        isdelete = YES;
        [alert dismissWithClickedButtonIndex:1 animated:NO];
        [self generateCSV];
    }
}

-(void)generateCSV
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    if ([filemgr fileExistsAtPath:[NSString stringWithFormat:@"%@/test.csv", docsDir] ] == NO)
    {
        [filemgr createFileAtPath:[NSString stringWithFormat:@"%@/test.csv", docsDir] contents:[@"" dataUsingEncoding: NSUnicodeStringEncoding] attributes: nil];
    }
    
    surveyarray = [db retrieveSurvey];
    
    CHCSVWriter * csvWriter = [[CHCSVWriter alloc] initWithCSVFile:[NSString stringWithFormat:@"%@/test.csv", docsDir] atomic:NO];
    
    [csvWriter writeField:@"No"];
    [csvWriter writeField:@"Date"];
    [csvWriter writeField:@"Purpose"];
    [csvWriter writeField:@"Individual"];
    [csvWriter writeField:@"Division"];
    [csvWriter writeField:@"Rating"];
    [csvWriter writeField:@"Feedback"];
    [csvWriter writeLine];
    
    for(int i = 0; i < surveyarray.count; i++)
    {
        survey = [surveyarray objectAtIndex:i];
        [csvWriter writeField:[NSString stringWithFormat:@"%d", i + 1]];
        [csvWriter writeField:survey.createddate];
        [csvWriter writeField:survey.question1];
        [csvWriter writeField:survey.question2];
        [csvWriter writeField:survey.division];
        [csvWriter writeField:survey.question3];
        [csvWriter writeField:survey.comment];
        [csvWriter writeLine];
    }
    
    if ([MFMailComposeViewController canSendMail])
    {
        if([GlobalFunction NetworkStatus])
        {
            
            NSMutableString *subject = [[NSMutableString alloc] init];
            NSMutableString *emailBody = [[NSMutableString alloc] init];
            
            [subject appendString:@"HR Customer Experience Report"];
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:subject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            NSData *myData = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/test.csv", docsDir]];
            //NSLog(@"%@", myData);
            
            [mailer addAttachmentData:myData mimeType:@"text/csv" fileName:@"OCBC_HRUE_DB.csv"];
            
            [mailer setMessageBody:emailBody isHTML:NO];
            
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
            [subject release];
            [emailBody release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"No Internet Connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            [alert release];
        }
    }
    else
    {
        isdelete = NO;
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [alert show];
        [alert release];
    }
    
    [csvWriter release];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
    if(result == 2)
    {
        if(isdelete)
        {
            [db deleteSurvey];
            isdelete = NO;
        }
        appdelegate.numberofswipe = 0;
        [secretbgview removeFromSuperview];
    }
}

@end

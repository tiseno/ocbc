//
//  MainScene.m
//  OCBC
//
//  Created by tiseno on 12/13/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "MainScene.h"

@implementation MainScene

@synthesize landscape, portrait;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
    appdelegate.noofquestion = 1;
    appdelegate.selection = -1;
    
    if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
    {
        LandscapeScene *tLandScapeViewController =[[LandscapeScene alloc] initWithNibName:@"LandscapeScene" bundle:nil];
        self.landscape=tLandScapeViewController;
        [tLandScapeViewController release];
        [self.view addSubview:self.landscape.view];
    }
    else
    {
        PortraitScene *tPortraitViewController =[[PortraitScene alloc] initWithNibName:@"PortraitScene" bundle:nil];
        self.portrait=tPortraitViewController;
        [tPortraitViewController release];
        [self.view addSubview:self.portrait.view];
    }
}

-(void)dealloc
{
    [landscape release];
    [portrait release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        LandscapeScene *tLandScapeViewController =[[LandscapeScene alloc] initWithNibName:@"LandscapeScene" bundle:nil];
        self.landscape=tLandScapeViewController;
        [self.portrait.view removeFromSuperview];
        [tLandScapeViewController release];
        [self.view addSubview:self.landscape.view];
    }
    else if(toInterfaceOrientation==UIInterfaceOrientationPortrait || toInterfaceOrientation==UIInterfaceOrientationPortraitUpsideDown)
    {
        PortraitScene *tPortraitViewController =[[PortraitScene alloc] initWithNibName:@"PortraitScene" bundle:nil];
        self.portrait = tPortraitViewController;
        [self.landscape.view removeFromSuperview];
        [tPortraitViewController release];
        [self.view addSubview:self.portrait.view];
    }
}

@end

//
//  AppDelegate.h
//  OCBC
//
//  Created by tiseno on 12/12/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSString *question, *answer, *division;
    NSNumber *answerindexselected;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic) int noofquestion, noofanswer, numberofswipe, selection;

@property (nonatomic, retain) NSMutableArray *questiontext, *answertext, *answerselected, *divisiontext;

@property (nonatomic, retain) NSString *comment;

@end

//
//  DivisionCell.m
//  OCBC
//
//  Created by tiseno on 12/18/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "DivisionCell.h"

@implementation DivisionCell

@synthesize titlelabel, tickimageview;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 260, 37)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:14];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.titlelabel = Title;
        [Title release];
        [self addSubview:titlelabel];
        
        UIImageView *Tickimageview = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tick.png"]];
        Tickimageview.frame = CGRectMake(self.frame.size.width - 10 - Tickimageview.image.size.width, (self.frame.size.height - Tickimageview.image.size.height) / 2 , Tickimageview.image.size.width, Tickimageview.image.size.height);
        Tickimageview.hidden = YES;
        self.tickimageview = Tickimageview;
        [Tickimageview release];
        [self addSubview:self.tickimageview];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

-(void)dealloc
{
    [titlelabel release];
    [tickimageview release];
    
    [super dealloc];
}

@end

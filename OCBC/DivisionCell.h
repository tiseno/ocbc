//
//  DivisionCell.h
//  OCBC
//
//  Created by tiseno on 12/18/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DivisionCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *titlelabel;
@property (nonatomic, retain) UIImageView *tickimageview;

@end

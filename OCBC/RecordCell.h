//
//  RecordCell.h
//  OCBC
//
//  Created by tiseno on 12/27/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface RecordCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *nolabel, *datelabel, *purposelabel, *individuallabel, *divisionlabel, *ratinglabel, *feedbacklabel;

@end

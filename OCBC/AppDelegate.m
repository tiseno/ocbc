//
//  AppDelegate.m
//  OCBC
//
//  Created by tiseno on 12/12/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

@synthesize noofquestion, noofanswer, numberofswipe, comment, selection;

@synthesize questiontext, answertext, answerselected, divisiontext;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.questiontext = [[[NSMutableArray alloc]init] autorelease];
    question = @"What is the purpose of your visit today?";
    [self.questiontext addObject:question];
    question = @"Please tell us more about yourself.";
    [self.questiontext addObject:question];
    question = @"Please rate your overall experience at HR.";
    [self.questiontext addObject:question];
    
    self.answertext = [[[NSMutableArray alloc]init] autorelease];
    answer = @"Interview";
    [self.answertext addObject:answer];
    answer = @"Enquiries";
    [self.answertext addObject:answer];
    answer = @"Meeting";
    [self.answertext addObject:answer];
    answer = @"Others";
    [self.answertext addObject:answer];
    answer = @"Employee";
    [self.answertext addObject:answer];
    answer = @"Visitor";
    [self.answertext addObject:answer];
    answer = @"Exceed Expectation";
    [self.answertext addObject:answer];
    answer = @"Meet Expectation";
    [self.answertext addObject:answer];
    answer = @"Under Expectation";
    [self.answertext addObject:answer];
    
    self.answerselected = [[[NSMutableArray alloc]init] autorelease];
    answerindexselected = [NSNumber numberWithInt:0];
    [self.answerselected addObject:answerindexselected];
    answerindexselected = [NSNumber numberWithInt:0];
    [self.answerselected addObject:answerindexselected];
    answerindexselected = [NSNumber numberWithInt:0];
    [self.answerselected addObject:answerindexselected];
    
    self.divisiontext = [[[NSMutableArray alloc]init] autorelease];
    division = @"Global Consumer Financial Services";
    [self.divisiontext addObject:division];
    division = @"Global Corporate Bank";
    [self.divisiontext addObject:division];
    division = @"Global Investment Banking";
    [self.divisiontext addObject:division];
    division = @"Global Treasury";
    [self.divisiontext addObject:division];
    division = @"Group Audit";
    [self.divisiontext addObject:division];
    division = @"Group Corporate Communications";
    [self.divisiontext addObject:division];
    division = @"Group Customer Experience";
    [self.divisiontext addObject:division];
    division = @"Group Finance";
    [self.divisiontext addObject:division];
    division = @"Group Legal & Regulatory Compliance";
    [self.divisiontext addObject:division];
    division = @"Group Operations & Technology";
    [self.divisiontext addObject:division];
    division = @"Group Property Management";
    [self.divisiontext addObject:division];
    division = @"Group Quality & Service Excellence";
    [self.divisiontext addObject:division];
    division = @"Group Risk Management";
    [self.divisiontext addObject:division];
    division = @"Group Secretariat";
    [self.divisiontext addObject:division];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];

    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

//
//  PortraitScene.h
//  OCBC
//
//  Created by tiseno on 12/13/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h> 
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import "Toast+UIView.h"
#import "DivisionCell.h"
#import "RecordCell.h"
#import "DatabaseAction.h"
#import "Survey.h"
#import "CHCSVWriter.h"
#import "GlobalFunction.h"

@interface PortraitScene : UIViewController<UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>
{
    UIView *questionview, *answerview, *secretbgview, *passwordview;
    UIImageView *logoimage;
    UIButton *previousbutton, *nextbutton, *donebutton, *homebutton;
    UITextView *commenttextview;
    UITextField *passwordtextview;
    
    AppDelegate *appdelegate;
    
    CGAffineTransform questiontransform, answertransform;
    
    int currentselection;
    
    BOOL isthankyou, isdone, isdraw, isdelete;
    
    DivisionCell *divisioncell;
    
    RecordCell *recordcell;
    
    UITableView *divisiontableview, *recordtableview;
    
    DatabaseAction *db;
    
    Survey *survey;
    
    NSArray *surveyarray;
    
    UIAlertView *alert;
}

@property (retain, nonatomic) IBOutlet UIView *employeepopview;

@property (strong, nonatomic) ViewController *viewController;

@end

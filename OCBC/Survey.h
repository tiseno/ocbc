//
//  Survey.h
//  OCBC
//
//  Created by tiseno on 12/19/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Survey : NSObject
{
    
}

@property (nonatomic, retain) NSString *createddate, *question1, *question2, *division, *question3, *comment;

@end

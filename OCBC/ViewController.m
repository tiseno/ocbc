//
//  ViewController.m
//  OCBC
//
//  Created by tiseno on 12/12/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([UIApplication sharedApplication].statusBarOrientation == 3 || [UIApplication sharedApplication].statusBarOrientation == 4)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"splash_screen_h.png"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"splash_screen_v.png"]];
    }
    
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singletapcaptured:)];
    [self.view addGestureRecognizer:singletap];
    [self.view setUserInteractionEnabled:YES];
    [singletap release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"splash_screen_h.png"]];
    }
    else if(toInterfaceOrientation==UIInterfaceOrientationPortrait || toInterfaceOrientation==UIInterfaceOrientationPortraitUpsideDown)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"splash_screen_v.png"]];
    }
}

-(void) singletapcaptured:(UIGestureRecognizer *)gesture
{
    [self gotoandplay];
}

-(void)gotoandplay
{
    MainScene *mainscene =[[MainScene alloc] initWithNibName:@"MainScene" bundle:nil];
    [self presentModalViewController:mainscene animated:YES];
    [mainscene release];
}

@end
